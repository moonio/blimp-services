'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    config = require('./../app/config/config'),
    helper = require('./../utils/test-helper'),
    app = require('./../app/app'),
    Log = mongoose.model('Log');

/**
 * Globals
 */
var log1,
    props = Object.keys(Log.schema.paths);

props = _.filter(props, function (prop) {
    return prop[0] !== '_';
});

/**
 * Test Suites
 */
describe('<Integration Test> Log routes', function() {
    describe('Start', function() {
        before(function(done) {
            log1 = {
                message: 'log_' + helper.getRandomString()
            };

            mongoose.connection.collections.logs.drop(function() {
                done();
            });
        });

        it('should begin without the testing log', function(done) {
            // find log one to test if it really isn't there
            Log.find({
                message: log1.message
            }, function(err, logs) {
                logs.should.have.length(0);
                done();
            });
        });
    });

    describe('Schema', function () {
        it('should return the log\'s schema', function (done) {
            request(app)
                .get('/api/v1/logs/schema')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');
                    res.body.should.have.property('resource', 'Log');
                    res.body.should.have.property('fields');
                    res.body.fields.should.have.property('_id');
                    _.each(props, function (prop) {
                        res.body.fields.should.have.property(prop);
                    });
                    done();
                });
        });
    });

    describe('Create', function() {
        beforeEach(function(done) {
            log1 = {
                message: 'log_' + helper.getRandomString()
            };
            done();
        });

        it('should add a new log', function(done) {
            // send creation request to the server
            request(app)
                .post('/api/v1/logs/')
                .send(log1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify result
                    res.should.have.property('status', 201);
                    res.should.have.property('body');
                    res.body.should.be.type('object');
                    res.body.message.should.equal(log1.message);

                    // verify database
                    Log.find({
                        _id: res.body._id
                    }, function(err, logs) {
                        logs.should.have.length(1);
                        done();
                    });
                });
        });

        it('should fail creating an log without a message', function (done) {
            log1.message = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/logs/')
                .send(log1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('message');
                    res.body.errors.message.should.have.property('path', 'message');
                    done();
                });
        });
    });

    describe('Read', function() {
        beforeEach(function(done) {
            log1 = {
                message: 'log_' + helper.getRandomString()
            };
            done();
        });

        it('should get logs', function(done) {
            // fetch logs
            request(app)
                .get('/api/v1/logs/')
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');
                    done();
            });
        });

        it('should get a specific log', function (done) {
            // create an log to fetch
            var _log = new Log(log1);
            _log.save(function(err) {
                should.not.exist(err);

                // fetch the log
                request(app)
                    .get('/api/v1/logs/' + _log._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body._id.should.equal(_log._id + '');
                        done();
                    });
            });
        });
    });
});
