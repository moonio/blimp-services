'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    Organisation = mongoose.model('Organisation'),
    helper = require('./../utils/test-helper');

/**
 * Globals
 */
var organisation1, organisation2;

/**
 * Test Suites
 */
describe('<Unit Test> Organisation model:', function() {

    before(function(done) {
        organisation1 = {
            name: 'org_' + helper.getRandomString()
        };

        organisation2 = {
            name: 'org_' + helper.getRandomString()
        };

        done();
    });

    describe('Method Save', function() {
        it('should begin without the test organisation', function(done) {
            Organisation.find({
                name: organisation1.name
            }, function(err, organisations) {
                organisations.should.have.length(0);

                Organisation.find({
                    name: organisation2.name
                }, function(err, organisations) {
                    organisations.should.have.length(0);
                    done();
                });
            });
        });

        it('should be able to save without problems', function(done) {
            var _organisation = new Organisation(organisation1);
            _organisation.save(function(err) {
                should.not.exist(err);
                _organisation.remove(function() {
                    done();
                });
            });
        });

        it('should be able to create organisation and save organisation for updates without problems', function(done) {
            var _organisation = new Organisation(organisation1);
            var newOrgName = 'newOrgName' + helper.getRandomString();
            _organisation.save(function(err) {
                should.not.exist(err);
                _organisation.name = newOrgName;
                _organisation.save(function(err) {
                    should.not.exist(err);
                    _organisation.name.should.equal(newOrgName);
                    _organisation.remove(function() {
                        done();
                    });
                });
            });
        });

        it('should fail to save an existing organisation with the same values', function(done) {
            var _organisation1 = new Organisation(organisation1);
            _organisation1.save();

            var _organisation2 = new Organisation(organisation1);

            return _organisation2.save(function(err) {
                should.exist(err);
                _organisation1.remove(function() {
                    if (!err) {
                        _organisation2.remove(function() {
                            done();
                        });
                    }
                    done();
                });
            });
        });

        it('should show an error when try to save without name', function(done) {
            var _organisation = new Organisation(organisation1);
            _organisation.name = '';
            return _organisation.save(function(err) {
                should.exist(err);
                done();
            });
        });
    });

    after(function(done) {

        /** Clean up organisation objects
         * un-necessary as they are cleaned up in each test but kept here
         * for educational purposes
         *
         *  var _organisation1 = new Organisation(organisation1);
         *  var _organisation2 = new Organisation(organisation2);
         *
         *  _organisation1.remove();
         *  _organisation2.remove();
         */

        done();
    });
});
