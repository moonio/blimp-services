'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    config = require('./../app/config/config'),
    helper = require('./../utils/test-helper'),
    app = require('./../app/app'),
    Organisation = mongoose.model('Organisation');

/**
 * Globals
 */
var organisation1,
    props = Object.keys(Organisation.schema.paths);

props = _.filter(props, function (prop) {
    return prop[0] !== '_';
});

/**
 * Test Suites
 */
describe('<Integration Test> Organisation routes', function() {
    describe('Start', function() {
        before(function(done) {
            organisation1 = {
                name: 'org_' + helper.getRandomString()
            };

            mongoose.connection.collections.organisations.drop(function() {
                mongoose.connection.collections.organisation_versions.drop(function() {
                    done();
                });
            });
        });

        it('should begin without the testing organisation', function(done) {
            // find organisation one to test if it really isn't there
            Organisation.find({
                name: organisation1.name
            }, function(err, organisations) {
                organisations.should.have.length(0);
                done();
            });
        });
    });

    describe('Schema', function () {
        it('should return the organisation\'s schema', function (done) {
            request(app)
                .get('/api/v1/organisations/schema')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');
                    res.body.should.have.property('resource', 'Organisation');
                    res.body.should.have.property('fields');
                    res.body.fields.should.have.property('_id');
                    _.each(props, function (prop) {
                        res.body.fields.should.have.property(prop);
                    });
                });
            done();
        });
    });

    describe('Create', function() {
        beforeEach(function(done) {
            organisation1 = {
                name: 'org_' + helper.getRandomString()
            };
            done();
        });

        it('should add a new organisation', function(done) {
            // send creation request to the server
            request(app)
                .post('/api/v1/organisations/')
                .send(organisation1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify result
                    res.should.have.property('status', 201);
                    res.should.have.property('body');
                    res.body.should.be.type('object');
                    res.body.name.should.equal(organisation1.name);

                    // verify database
                    Organisation.find({
                        _id: res.body._id
                    }, function(err, organisations) {
                        organisations.should.have.length(1);
                        done();
                    });
                });
        });

        it('should fail creating an organisation that already exists', function (done) {
            var _organisation = new Organisation(organisation1);
            _organisation.save(function(err) {
                should.not.exist(err);

                var cloneOrg = _.clone(organisation1);

                // send creation request to the server
                request(app)
                    .post('/api/v1/organisations/')
                    .send(cloneOrg)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        res.should.have.property('status', 400);
                        res.should.have.property('body');
                        res.body.should.have.property('name', 'ValidationError');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('name');
                        res.body.errors.name.should.have.property('path', 'name');

                        done();
                    });
            });
        });

        it('should fail creating an organisation without a name', function (done) {
            organisation1.name = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/organisations/')
                .send(organisation1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('name');
                    res.body.errors.name.should.have.property('path', 'name');

                    done();
                });
        });
    });

    describe('Read', function() {
        beforeEach(function(done) {
            organisation1 = {
                name: 'org_' + helper.getRandomString()
            };
            done();
        });

        it('should get organisations', function(done) {
            // fetch organisations
            request(app)
                .get('/api/v1/organisations/')
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');

                    done();
            });
        });

        it('should get a specific organisation', function (done) {
            // create an organisation to fetch
            var _organisation = new Organisation(organisation1);
            _organisation.save(function(err) {
                should.not.exist(err);

                // fetch the organisation
                request(app)
                    .get('/api/v1/organisations/' + _organisation._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body._id.should.equal(_organisation._id + '');

                        done();
                    });
            });
        });
    });

    describe('Update', function() {
        beforeEach(function(done) {
            organisation1 = {
                name: 'org_' + helper.getRandomString()
            };
            done();
        });

        it('should correctly update an existing organisation', function (done) {
            // create an organisation to update
            var _organisation = new Organisation(organisation1);
            _organisation.save(function(err) {
                should.not.exist(err);

                // update a field
                organisation1.logo = helper.getRandomAvatar();

                // send the update to the server
                request(app)
                    .put('/api/v1/organisations/' + _organisation._id)
                    .send({
                        logo: organisation1.logo
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body.should.have.property('_id', _organisation._id + '');
                        res.body.should.have.property('logo', organisation1.logo);

                        // verify the database
                        Organisation.find({
                            _id: _organisation._id
                        }, function(err, organisations) {
                            organisations.should.have.length(1);
                            organisations[0].should.have.property('logo', organisation1.logo);
                            done();
                        });
                    });
            });
        });
    });

    describe('Delete', function() {
        beforeEach(function(done) {
            organisation1 = {
                name: 'org_' + helper.getRandomString()
            };
            done();
        });

        it('should remove an existing organisation without problem', function (done) {
            // create an organisation to delete
            var _organisation = new Organisation(organisation1);
            _organisation.save(function(err) {
                should.not.exist(err);

                // send deletion request to the server
                request(app)
                    .delete('/api/v1/organisations/' + _organisation._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify result
                        res.should.have.property('status', 204);

                        // verify database
                        Organisation.findById(_organisation._id, function(err, organisation) {
                            should.not.exist(err);
                            should.exist(organisation, 'Organisation doesn\'t exist');
                            organisation.should.have.property('isDeleted', true);
                            done();
                        });
                    });
            });
        });
    });

    describe('Organisation History', function () {
        var _organisation;

        before(function (done) {
            organisation1 = {
                name: 'org_' + helper.getRandomString()
            };
            _organisation = new Organisation(organisation1);
            _organisation.save(function(err) {
                done();
            });
        });

        it('should be empty for a new organisation', function (done) {
            request(app)
                .get('/api/v1/organisations/' + _organisation._id + '/history')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body', []);
                    done();
                });
        });

        it('should be have a change for each modification to the organisation', function (done) {
            var old = JSON.parse(JSON.stringify(organisation1));
            _organisation.set({
                name: 'newName'
            });

            _organisation.save(function(err, org) {
                request(app)
                    .get('/api/v1/organisations/' + _organisation._id + '/history?populate=_refId')
                    .end(function(err, res) {
                        if(err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body');
                        res.body.length.should.equal(1);
                        _.each(props, function (prop) {
                            if(old[prop]) {
                                res.body[0].should.have.property(prop, old[prop]);
                            }
                        });
                        res.body[0].should.have.property('_action', 'save');
                        done();
                    });
            });
        });
    });
});
