#!/bin/sh
#start our blimp-App

g='\033[0;32m' # Green
lg='\033[1;32m' # Light Green
lga='\033[0;37m' # Light Gray
NC='\033[0m' # No Color

echo "${g}"
cat public/.logo
echo "${lg}"
cat public/.blimp
echo "${g}"
echo "           +---------------------------------------------------------------+"
echo "           |                  ${lg}Installing NPM dependencies${g}                  |"
echo "           +---------------------------------------------------------------+"
echo "${lga}"
npm install
echo "${g}"
echo "           +---------------------------------------------------------------+"
echo "           |                  ${lg}Installing Bower Components${g}                  |"
echo "           +---------------------------------------------------------------+"
bower install
echo "${g}"
echo "           +---------------------------------------------------------------+"
echo "           |                   ${lg}Running some grunt tasks${g}                    |"
echo "           +---------------------------------------------------------------+"
grunt dist
echo "${g}"
echo "           +---------------------------------------------------------------+"
echo "           |                  ${lg}Start with some dummy data${g}                   |"
echo "           +---------------------------------------------------------------+"
echo "\n"
echo "                  If this is the first time running blimp, you might       "
echo "                 want to start with some dummy data. Try running the       "
echo "\n"
echo "                                 ${lga}'npm run fixtures'${g}              "
echo "\n"
echo "                          command in a new terminal window                 "
echo "\n"
echo "           +---------------------------------------------------------------+"
echo "           |                          ${lg}Running App${g}                          |"
echo "           +---------------------------------------------------------------+"
echo "${NC}"
node_modules/.bin/supervisor -eq 'js|json' app
