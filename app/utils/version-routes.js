'use strict';
var uberquery = require('uberquery'),
    async = require('async'),
    _ = require('lodash'),
    moment = require('moment');

var addVersionRoutes = function addVersionRoute(router, Model, pathPrefix, versionModelName) {
    versionModelName = versionModelName || Model.modelName + '_version';

    var Entity = uberquery.mongoose.model(Model.modelName);
    var EntityVersions = uberquery.mongoose.model(versionModelName);

    // /users/:id/history
    Model.route('history', {
        detail: true,
        handler: function(req, res, next) {
            var query = uberquery.query(req, versionModelName);
            query._conditions._refId = req.params.id;

            uberquery.handlers.get(req, res, next);
        },
        methods : ['get']
    });

    // /users/:id/historyat/:ts
    router.get(pathPrefix + '/:id/historyat/:ts', function (req, res, next) {
        var params = req.params;

        var requestedTimestamp = moment(params.ts);

        if (!requestedTimestamp.isValid()) {
            uberquery.handlers.respond(res, 400, {
                msg: 'parameter mismatch: invalid timestamp',
                description: 'Any timestamp older than the time of request is accepted. Any format accepted by momentjs (http://momentjs.com/docs/#/parsing/string/) is also valid for this request.'
            });
            uberquery.handlers.send(req, res, next);
            return;
        }

        async.parallel({
            model: function(callback){
                var qry = Entity.findById(params.id);

                qry.exec(function(err, entity) {
                    callback(err, entity);
                });
            },
            versions: function(callback){
                var qry = EntityVersions.find({
                    _refId: params.id,
                    _modified: { $lte: params.ts }
                }).sort('-_modified').limit(1);

                qry.exec(function(err, versions) {
                    callback(err, versions);
                });
            }
        },
        function(err, results) {
            // get out if an error occured
            if (err) {
                uberquery.handlers.respond(res, 500, err);
                uberquery.handlers.send(req, res, next);
                return;
            }

            var model = results.model;
            var versions = results.versions;

            // get out if the model does not exist
            if (!model) {
                uberquery.handlers.respond(res, 404, {msg: 'parameter mismatch: model [id: ' + params.id + '] not found'});
                uberquery.handlers.send(req, res, next);
                return;
            }

            // create an array ordered by timestamp
            var arr = _.map(versions, function mapVersions(version) {
                return {
                    timestamp: version._modified,
                    model: version
                };
            });

            // add the model to the end of the array (model is latest version)
            arr.push({
                timestamp: model._modified,
                model: model
            });

            // get the version at the timestamp.
            arr = _.filter(arr, function removeNewerItems(obj) {
                return obj.timestamp <= requestedTimestamp;
            });

            var latestVersionAtRequestedTimeStamp = _.last(arr);

            if (!latestVersionAtRequestedTimeStamp) {
                uberquery.handlers.respond(res, 404, {msg: "model was not created at requested time!"});
            } else {
                uberquery.handlers.respond(res, 200, latestVersionAtRequestedTimeStamp.model);
            }
            uberquery.handlers.send(req, res, next);
        });
    });
};

exports = module.exports = addVersionRoutes;
