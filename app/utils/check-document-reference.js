'use strict';
var _ = require('lodash');

module.exports = function (ModelSchema, property) {
    if (!property) {
        property = '_id';
    }

    var filter = {};

    return function (value, respond) {

        if (!Array.isArray(value)) {
            value = [value];
        }

        _.uniq(value);

        filter[property] = {
            $in: value
        };

        ModelSchema.find(filter, function (err, doc) {
            respond(!err && doc.length !== filter[property.length]);
        });
    };
};
