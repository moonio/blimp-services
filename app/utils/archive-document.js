'use strict';

var uberquery = require('uberquery'),
    ObjNotFound = require('./object-not-found');

module.exports = function(req, res, next) {
    var params = req.params;
    var Model = req.quer.model;

    Model.findById(params.id, function(err, document) {
        if(err) {
            uberquery.handlers.respond(res, 404, ObjNotFound);
            uberquery.handlers.send(req, res, next);
        }

        document.set({deletedAt: new Date(), isDeleted: true});

        document.save(function(err) {
            uberquery.handlers.respondOrErr(res, 500, err, 204, undefined);
            uberquery.handlers.send(req, res, next);
        });
    });
};
