'use strict';

module.exports  = function(req, res, next) {
    if(!req.params.id) {
        var _conditions = req.quer._conditions;
        if(_conditions.isDeleted === undefined) {
            _conditions.isDeleted = { $in: [false, null] };
        }
    }
    next();
};
