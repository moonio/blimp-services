'use strict';

var fs = require('fs'),
    express = require('express');

// Initialize application routes
function initAppRoutes(app, router) {

    // default route
    router.get('/', function(req, res) {
        res.render('index', {});
    });

    router.get('/api', function(req, res) {
        res.render('index', {});
    });

    router.get('/api/:version', function(req, res) {
        res.render('index', {apiVersion: req.params.version});
    });

    return router;
}

// Initialize the API
function initApiRoutes(app, router) {
    var routeDirectoryPath = app.get('appdir') + '/routes/';

    // register API routes
    if (fs.existsSync(routeDirectoryPath)) {
        var routeDirectory = fs.readdirSync(routeDirectoryPath);

        routeDirectory.forEach(function(routerName) {
            // cut of the '.js' part
            var first = 0;
            routerName = routerName.split('.')[first];
            // don't have to load the api file twice so skip it
            if (routerName === 'api') {
                return;
            }
            require(routeDirectoryPath + routerName)(router);
        });
    }
}

// Initialize the Routes
function init(app) {
    var appRouter = express.Router(),
        apiRouter = express.Router();

    appRouter = initAppRoutes(app, appRouter);
    app.use('/', appRouter);

    initApiRoutes(app, apiRouter);
    app.use('/api/v1/', apiRouter);
}

// return the router
module.exports = init;
