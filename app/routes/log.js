'use strict';

var Log = require('./../models/Log'),
    path = '/logs';

module.exports = function(router) {
    // Default routes
    Log = Log.methods(['get', 'post']);

    Log.register(router, path);
};
