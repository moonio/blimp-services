'use strict';

var express = require('express'),
    morgan = require('morgan'),
    uuid = require('node-uuid'),
    app = express(),
    swig = require('swig'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    apiRouter = require('./routes/api'),
    uberquery = require('uberquery'),
    config,
    router;

// Load configurations
// Set the node environment variable if not set before
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// App Path
app.set('appdir', __dirname);

// Initialize View System
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', app.get('appdir') + '/views');
app.set('view cache', false);
swig.setDefaults({
    cache: false
});

// Initialize cors
app.use(cors());

// Initialize static folder for assets
app.use('/assets', express.static('public'));

// Initializing system variables
config = require('./config/config');

// Initialize database
mongoose.connect(config.db);

// Initialize UberQuery
uberquery.init(mongoose);

// Initializing logger
var reporter = (process.env.NODE_ENV === 'development') ? 'dev' : 'combined';
if(process.env.NODE_ENV !== 'test') {
    app.use(morgan(reporter));
}

// Initialize our app
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// define App routes
apiRouter(app);

// launch our blimp server
app.listen(config.port);

// Expose app module to our app
exports = module.exports = app;
