'use strict';

module.exports = {
    db: 'mongodb://localhost:27017/blimp-test',
    port: 5002,
    logToConsole: false,
    app: {
        name: 'Blimp - Test'
    }
};
