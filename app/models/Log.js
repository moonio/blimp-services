'use strict';

var mongoose = require('mongoose'),
    checkDocumentReference = require('./../utils/check-document-reference'),
    Schema = mongoose.Schema,
    restful = require('node-restful'),
    version = require('mongoose-version2'),
    config = require('./../config/config'),
    UserModel = require('./User');

var LogSchema = new Schema({
    category: {
        type: ['string']
    },
    message: {
        type: 'string',
        required: true,
        index: true
    },
    user: {
        type: 'ObjectId',
        ref: 'User'
    }
});

LogSchema.path('user').validate(checkDocumentReference(UserModel), 'Log.user does not exist');

LogSchema.plugin(version, {});

module.exports = restful.model('Log', LogSchema);
