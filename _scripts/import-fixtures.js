'use strict';

var fixtures = require('pow-mongoose-fixtures'),
    mongoose = require('mongoose'),
    AsciiBanner = require('ascii-banner'),
    logger = require('blimp-logger'),
    express = require('express'),
    app = express(),
    fs = require('fs'),
    config,
    db;

// Load configurations
// Set the node environment variable if not set before
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Initializing system variables
config = require('./../app/config/config');

// Initialize database
db = mongoose.connect(config.db);

// Initializing logger
logger.initialize(app, mongoose);

logger.log('Importing Blimp fixtures', 'fixtures');

// load models
var modelPath = __dirname + '/../app/models/';
if (fs.existsSync(modelPath)) {
    var modelDir = fs.readdirSync(modelPath);

    modelDir.forEach(function(modelName) {
        modelName = modelName.split('.')[0];
        var modelSchema = require(modelPath + modelName);
        var Model = mongoose.model(modelName, modelSchema);
    });
}

// Initialize import output

function startImport() {
    // Import data
    fixtures.load(__dirname + '/data/data.js', db, function afterImport() {
        // Close the connection
        mongoose.connection.close();

        console.log('Thanks for importing, BYE!');
//        AsciiBanner.reset().write('Bye!', 'red').out(function() {
            logger.log('completed importing Blimp fixtures', 'fixtures');
//        });
    });
}

AsciiBanner
    .write('BLIMP')
    .color('green')
    .font('starwars').after(1)
    .after('>v{{version}}<', 'white')
    .after(3)
    .out(startImport);

