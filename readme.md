#Services

##Getting started

Fire the app on a unix based system  

    npm start

Fire the app on a windows system.  

    npm run startwin

Install all dependencies (NPM and Bower), this happens automatically on `npm start`.

    npm run prep

##Importing Fixtures

Kickstart your database with some dummy data to start with...  
Run `npm run fixtures` to install some users, projects, 
organisations and workitems in your database

##Usage

**Check out the [Wiki](/moonio/blimp-services/wiki) for all available endpoints and the parameters they require.**

##Running tests

To run all unit tests:

    npm test

optionally, code coverage can be generated during unit tests:

```bash
npm test --coverage
#or
npm run cover
```

##Got Issues?

Missing something? Got a cool idea? Use the
[Issues](/moonio/blimp-services/issues/?status=new&status=open)
section of this repository.
